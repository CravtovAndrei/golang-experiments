package tools

import (
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
)

func SaveData(path string, fileName string, data []byte, result chan<- string) {

	exists := CheckIfFolderExists(path)
	var fileRights fs.FileMode = 0644
	var folderRights fs.FileMode = 0777

	if exists {
		err := ioutil.WriteFile(path+fileName, data, fileRights)

		if err != nil {
			panic(fmt.Errorf("Something happened during file save %w \n", err))
		}
		result <- "Folder exists already"
		return
	}
	if !exists {
		err := os.Mkdir(path, folderRights)

		if err != nil {
			panic(fmt.Errorf("Someting happened during folder creation %w \n", err))
		}

		err = ioutil.WriteFile(path+fileName, data, fileRights)

		if err != nil {
			panic(fmt.Errorf("Someting happened during file save %w \n", err))
		}
		result <- "Created and saved file"
		return
	}

	result <- "Could not save the file"
}

func CheckIfFolderExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}

	if err != nil {
		return false
	}

	return false
}
