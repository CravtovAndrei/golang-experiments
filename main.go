package main

import (
	"fmt"
	"gitlab.com/CravtovAndrei/golang-experiments/appConfig"
	"gitlab.com/CravtovAndrei/golang-experiments/tools"
	"gitlab.com/CravtovAndrei/golang-experiments/websiteLocal"
	"io/ioutil"
	"net/http"
)

func main() {
	config := appConfig.ConfigureApplication()
	linkInfo := config.LINKINFO[0]["linkinfo"]
	currentPort := config.CURRENTPORT[1]["currentPort"]
	htmlTemplates := config.HTMLTEMPLATES[2]["htmlTemplates"]
	htmlTemplatesBlocks := config.HTMLTEMPLATEBLOCKS[3]["htmlTemplateBlocks"]

	downloadChannelResult := make(chan string)

	response, respErr := http.Get(linkInfo.URL)

	if respErr != nil {
		fmt.Println(" No response was received ")
	}

	defer response.Body.Close()

	responseBody, err := ioutil.ReadAll(response.Body)

	if err != nil {
		panic(fmt.Errorf("Something happened during json read %w \n", err))
	}

	go tools.SaveData("./downloads/", "downloaded.json", responseBody, downloadChannelResult)
	fmt.Println(<-downloadChannelResult)

	websiteLocal.StartWebsite(currentPort.PORTNUMBER, currentPort.DATABASESOURCENAMEADRESS, htmlTemplates, htmlTemplatesBlocks)
}
