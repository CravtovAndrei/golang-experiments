package users

type User struct {
	Name    string
	Age     int
	Hobbies []string
}

func (u *User) SetUserName(name string) {
	u.Name = name
}

func (u *User) SetUserAge(age int) {
	u.Age = age
}

func (u *User) GetUserInfo() string {
	return "User name and age are " + u.Name + ", " + string(u.Age)
}
