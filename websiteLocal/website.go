package websiteLocal

import (
	"fmt"
	"html/template"
	"net/http"
	"time"

	"gitlab.com/CravtovAndrei/golang-experiments/appConfig"

	"github.com/gorilla/mux"
	"gitlab.com/CravtovAndrei/golang-experiments/users"

	"database/sql" //todo move SQL stuff to separated folder

	_ "github.com/go-sql-driver/mysql"
)

//vars
var posts = []ArticleInDB{}
var currentShownPost = ArticleInDB{}
var databaseSourceNameAdress = ""
var htmlTemplates appConfig.HtmlTemplates
var htmlTemplatesBlocks appConfig.HtmlTemplateBlocks

func StartWebsite(port string, dbSourceNameAdress string,
	htmlTempls appConfig.HtmlTemplates, htmlTemplsBlocks appConfig.HtmlTemplateBlocks) {

	htmlTemplates = htmlTempls
	htmlTemplatesBlocks = htmlTemplsBlocks

	router := mux.NewRouter()

	databaseSourceNameAdress = dbSourceNameAdress

	router.HandleFunc("/", index_page).Methods("GET")
	router.HandleFunc("/create", create_page).Methods("GET")
	router.HandleFunc("/post/{id:[0-9]+}", show_post).Methods("GET")
	router.HandleFunc("/contacts/", contacts_page).Methods("GET")
	router.HandleFunc("/save_article", save_page).Methods("POST")

	http.Handle("/", router)
	http.Handle("/css/", http.StripPrefix("/", http.FileServer(http.Dir("static"))))

	http.ListenAndServe(port, nil)
}

func OpenDataBase() *sql.DB {
	fmt.Println("Starting MySql")

	db, err := sql.Open("mysql", databaseSourceNameAdress)

	db.SetConnMaxLifetime(time.Second * 20)

	if err != nil {
		panic(err)
	}

	fmt.Println("Connected")

	return db
}

func InsertIntoUsersTable(db *sql.DB, name string, age uint16) {

	query := fmt.Sprintf("INSERT INTO users (name,age) VALUES ('%s','%d')", name, age)

	insertData, err := db.Query(query)

	if err != nil {
		panic(err)
	}

	defer insertData.Close()

	db.Close()
}

func InsertIntoArticlesTable(db *sql.DB, title string, anons string, fulltext string) {

	query := fmt.Sprintf("INSERT INTO articles (title,anons,alltext) VALUES ('%s','%s','%s')", title, title, title)

	insertData, err := db.Query(query)

	if err != nil {
		panic(err)
	}

	defer insertData.Close()

	db.Close()
}

func show_post(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)

	t, err := template.ParseFiles(htmlTemplates.SHOWPOSTPAGE, htmlTemplates.HEADERPAGE, htmlTemplates.FOOTERPAGE)

	w.WriteHeader(http.StatusOK)

	db := OpenDataBase()

	query := fmt.Sprintf("SELECT * FROM `articles` WHERE `id` = '%s'", vars["id"])

	result, err := db.Query(query)

	if err != nil {
		fmt.Println("Could not select from DataBase")
	}

	currentShownPost = ArticleInDB{}

	for result.Next() {
		var post ArticleInDB

		err = result.Scan(&post.Id, &post.Title, &post.Anons, &post.AllText)

		if err != nil {
			fmt.Println("Could not parse a post")
		}

		currentShownPost = post
	}

	t.ExecuteTemplate(w, htmlTemplatesBlocks.SHOWPOSTBLOCK, currentShownPost) //here template define block name

	defer db.Close()

}

func index_page(w http.ResponseWriter, req *http.Request) {

	t, err := template.ParseFiles(htmlTemplates.INDEXPAGE, htmlTemplates.HEADERPAGE, htmlTemplates.FOOTERPAGE)

	posts = []ArticleInDB{}

	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	db := OpenDataBase()

	result, err := db.Query("SELECT * FROM `articles`")

	if err != nil {
		fmt.Println("Could not select from DataBase")
	}

	for result.Next() {
		var post ArticleInDB

		err = result.Scan(&post.Id, &post.Title, &post.Anons, &post.AllText)

		if err != nil {
			fmt.Println("Could not parse a post")
		}

		posts = append(posts, post)

	}

	t.ExecuteTemplate(w, htmlTemplatesBlocks.INDEXBLOCK, posts) //here template define block name

	db.Close()
}

func create_page(w http.ResponseWriter, req *http.Request) {

	t, err := template.ParseFiles(htmlTemplates.CREATEPAGE, htmlTemplates.HEADERPAGE, htmlTemplates.FOOTERPAGE)

	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	t.ExecuteTemplate(w, htmlTemplatesBlocks.CREATEBLOCK, nil) //here template define block name
}

func save_page(w http.ResponseWriter, req *http.Request) {

	title := req.FormValue("title")
	anons := req.FormValue("anons")
	fullText := req.FormValue("full_text")

	if len(title) == 0 || len(anons) == 0 || len(fullText) == 0 {
		fmt.Println("Some of the string is empty")
		return
	}

	db := OpenDataBase()

	InsertIntoArticlesTable(db, title, anons, fullText)

	http.Redirect(w, req, "/", http.StatusSeeOther)
}

func home_page(page http.ResponseWriter, r *http.Request) {

	defaultUser := users.User{"Pascal", 23, []string{"Music", "Dance", "Driving"}}
	defaultUser.SetUserAge(20)
	defaultUser.SetUserName("Default")
	tmpl, _ := template.ParseFiles(htmlTemplates.HOMEPAGE)
	tmpl.Execute(page, defaultUser)
}

func contacts_page(page http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(page, "Contacts")
}

func information_page(page http.ResponseWriter, r *http.Request) {

}

//structs

type ArticleInDB struct {
	Id                    uint16
	Title, Anons, AllText string
}
