package appConfig

import (
	"fmt"
	"github.com/spf13/viper"
)

func ConfigureApplication() Api {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./configs/")

	var config Api

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Something happened during config read %w \n", err))
	}

	err = viper.Unmarshal(&config)

	if err != nil {
		panic(fmt.Errorf("Unable to decode the config %w \n", err))
	}

	return config
}

//names in these struct must match to yml

type Api struct {
	LINKINFO           []map[string]LinkInfo           `mapstructure:"api"`
	CURRENTPORT        []map[string]CurrentPort        `mapstructure:"api"`
	HTMLTEMPLATES      []map[string]HtmlTemplates      `mapstructure:"api"`
	HTMLTEMPLATEBLOCKS []map[string]HtmlTemplateBlocks `mapstructure:"api"`
}

type LinkInfo struct {
	URL  string `mapstructure:"url"`
	INFO string `mapstructure:"info"`
}

type CurrentPort struct {
	PORTNUMBER               string `mapstructure:"portNumber"`
	DATABASESOURCENAMEADRESS string `mapstructure:"databaseSourceNameAdress"`
}

type HtmlTemplates struct {
	HOMEPAGE     string `mapstructure:"homePage"`
	INDEXPAGE    string `mapstructure:"indexPage"`
	CREATEPAGE   string `mapstructure:"createPage"`
	HEADERPAGE   string `mapstructure:"headerPage"`
	FOOTERPAGE   string `mapstructure:"footerPage"`
	SHOWPOSTPAGE string `mapstructure:"showPostPage"`
}

type HtmlTemplateBlocks struct {
	HEADERBLOCK   string `mapstructure:"headerBlock"`
	INDEXBLOCK    string `mapstructure:"indexBlock"`
	CREATEBLOCK   string `mapstructure:"createBlock"`
	SHOWPOSTBLOCK string `mapstructure:"showPostBlock"`
}
